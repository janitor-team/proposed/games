#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
Copyright 2015, Markus Koschany <apo@debian.org>

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this package; if not, write to the Free Software Foundation, Inc., 51 Franklin
St, Fifth Floor, Boston, MA 02110-1301 USA
'''

from psycopg2 import connect

ALL_GAMES = "games_all"

def get_all_games():
    query ="""
        SELECT package
        FROM packages
        WHERE distribution='debian'
        AND section='games'
        AND release='sid'
        AND component='main'
        AND (architecture='amd64' OR architecture='all')
        ORDER BY package
    """;


    conn = connect(
            database='udd',
            port=5432,
            host='udd-mirror.debian.net',
            user='udd-mirror',
            password='udd-mirror')

    cur = conn.cursor()
    cur.execute(query)
    rows = cur.fetchall()
    cur.close()
    conn.close()

    return rows

def write_all_games_to_file():
    with open(ALL_GAMES, mode="w", encoding="utf-8") as fout:
        for row in get_all_games():
            fout.write("%s\n" % str(row[0]))

#get_all_games()
write_all_games_to_file()
