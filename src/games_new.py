#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
Copyright 2015, Markus Koschany <apo@debian.org>

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this package; if not, write to the Free Software Foundation, Inc., 51 Franklin
St, Fifth Floor, Boston, MA 02110-1301 USA
'''

NEW_GAMES = "games_new"
ALL_GAMES = "games_all"
CURRENT_GAMES = "games_current"
OLD_GAMES = "games_old"
BLACKLIST = "blacklist"
files = (OLD_GAMES, BLACKLIST)

def get_new_games():
    with open(CURRENT_GAMES, mode="w", encoding="utf-8") as f_current:
        for fname in files:
            with open(fname) as fin:
                f_current.write(fin.read())

    with open(CURRENT_GAMES) as f_current:
        s1 = set(f_current)

    with open(ALL_GAMES) as f_all:
        with open(NEW_GAMES, mode="w", encoding="utf-8") as f_new:
            f_new.writelines(x for x in f_all if x not in s1)

get_new_games()
