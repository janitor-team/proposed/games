debian-games (4) unstable; urgency=medium

  * Update the blacklist
  * New games:
    - puzzle: chroma
    - console,toys: cbonsai
  * Removed games or tools: (removed from Debian)
    - adanaxisgpl
    - aajm
    - jmdlx
    - fretsonfire
    - kiki-the-nano-bot
    - jerry
    - ninja-ide
    - python3-pyqt4.qtopengl

 -- Markus Koschany <apo@debian.org>  Sun, 04 Jul 2021 08:50:03 +0200

debian-games (3.3) unstable; urgency=medium

  * arcade: Remove fofix from Suggests.
  * java-dev: Remove netbeans. Removed from Debian.
  * Declare compliance with Debian Policy 4.5.1.
  * Migrate to udd-mirror.debian.net.
    Thanks to Asheesh Laroia for the patch. (Closes: #976060, 980922)
  * New games:
    - rpg: gm-assistant
    - content-dev: goverlay
    - chess: gpsshogi
    - chess: gpsshogi-viewer
    - tetris: gtetrinet
    - strategy: ironseed
    - board: kgames
    - rpg: openmw
    - rpg: openmw-cs
    - arcade: pinball-table-gnu
    - arcade: pinball-table-hurd
    - puzzle: puzzle-jigsaw
    - puzzle: sudoku-solver
    - console: trader
  * Update the blacklist and add braillefont, nethack-qt, ironseed-data,
    ktuberling-data, minigalaxy, because it is not a game and it is only able to
    download games from G.O.G.

 -- Markus Koschany <apo@debian.org>  Fri, 19 Feb 2021 01:29:47 +0100

debian-games (3.2) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.5.0.
  * Switch to debhelper-compat = 13.
  * New games or engines:
    - puzzle,console: 2048
    - adventure: dmagnetic
    - chess: fairy-stockfish
    - toys: macopix
    - console,typing: pacvim
    - adventure: sdlfrotz
    - arcade: vonsh
  * Removed games: (removed from Debian)
    - puzzle: 4digits
    - card: dds
    - platform: ffrenzy
    - puzzle: freevial
    - chess: gameclock
    - board: londonlaw
    - toys: macopix-gtk2
    - mud: mudlet
    - education, puzzle: pysycache
    - card: python-pydds
    - strategy: slingshot
    - board: uligo
    - arcade: xpilot-ng

 -- Markus Koschany <apo@debian.org>  Tue, 13 Oct 2020 11:28:42 +0200

debian-games (3.1) unstable; urgency=medium

  * Drop python2-dev package because Python 2 will be removed from Debian.
    (Closes: #945632)
  * Declare compliance with Debian Policy 4.4.1.
  * Removed games: (Removed from Debian)
    - arcade/shootemup: balder2d
    - arcade: bubbros
    - arcade/shootemup: defendguin
    - puzzle/board: gnudoq
    - board: gpsshogi
    - card: holdingnuts
    - puzzle: magicor
    - card: pybridge
    - puzzle: pynagram
    - board: pyscrabble
    - typing: speedpad
    - board: tictactoe-ng
    - puzzle: xword
    - console: zivot
  * New games or engines:
    - chess: suggest crazywa
    - adventure: add glulxe
    - platform/arcade: add pekka-kana-2
    - simulation: suggest pmars
    - console/tetris: add termtris
    - toys: add tfortune and tfortunes
    - toys: add xsnow

 -- Markus Koschany <apo@debian.org>  Tue, 17 Dec 2019 02:08:26 +0100

debian-games (3) unstable; urgency=medium

  * Suggest Netbeans, cuyo and holdingnuts because they will not be part of
    Debian 10 "Buster".

 -- Markus Koschany <apo@debian.org>  Mon, 20 May 2019 00:01:59 +0200

debian-games (2.6) unstable; urgency=medium

  * games-tasks: Depend on ${misc:Depends}
  * board: Remove gmchess from board task too.
  * Remove eclipse related packages because the Eclipse IDE was removed from
    Debian.
  * Update the blacklist
  * New games: python3-dev: Suggest python3-minecraftpi.
    - puzzle: lix
    - chess: jerry
    - chess: fathom
    - chess: ethereal-chess
    - emulator: blastem
  * Switch to compat level 12.
  * Declare compliance with Debian Policy 4.3.0.

 -- Markus Koschany <apo@debian.org>  Sun, 10 Feb 2019 14:13:56 +0100

debian-games (2.5) unstable; urgency=medium

  * Add basic256 also to the programming task.
  * Add python3-fife to python3-dev. Remove python2-fife.
  * Add vitetris to console and tetris tasks.
  * Declare compliance with Debian Policy 4.2.1.
  * New games:
    - rogue: boohu.
    - tetris: galois
    - toys: hollywood and wallstreet.
    - chess: Suggest leela-zero and liblizzie-java.
    - strategy: naev.
  * Removed games or libraries: (Removed from Debian)
    - attal
    - convert-pgn
    - eboard-extras-pack1
    - eleeye
    - glee-dev
    - gmchess
    - mmpong-gl

 -- Markus Koschany <apo@debian.org>  Sat, 08 Dec 2018 19:53:24 +0100

debian-games (2.4) unstable; urgency=medium

  * Move basic256 to education task.
  * Declare compliance with Debian Policy 4.2.0.
  * Add hyperrogue to finest games.
  * Remove finest-light binary package. Nowadays modern computers should be
    capable of running all free software games in Debian without restrictions.
  * New games:
     - fps: Suggested: crispy-doom
     - rogue: cataclysm-dda-sdl
     - strategy: gigalomania
     - puzzle: pushover

 -- Markus Koschany <apo@debian.org>  Fri, 03 Aug 2018 19:40:20 +0200

debian-games (2.3) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.1.4.
  * Update Vcs-fields. Git repository was moved to salsa.debian.org.
  * New games:
     - platform: ddnet.
     - racing: dustracing2d.
     - board: el-ixir.
     - console,shootemup: piu-piu.
  * Removed packages (removed from Debian):
     - ghextris, gtetrinet, gvrng, oolite.
  * Run make dist.

 -- Markus Koschany <apo@debian.org>  Sun, 15 Apr 2018 20:32:02 +0200

debian-games (2.2) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.1.2.
  * Switch to compat level 11.
  * Demote gnome-video-arcade to Suggests because it is a GNOME specific
    frontend.
  * New games:
    - arcade: icebreaker
    - rogue: allure
    - adventure: Replace fizmo-ncursesw with fizmo-sdl2 and demote
                 fizmo-ncursesw to Suggests.
    - toys: Suggest fortunate.app.
    - puzzle: jag.
    - finest: lugaru.
    - console: open-adventure.
    - strategy: planetblupi.
  * Removed packages: (removed from Debian)
    - pairs, linthesia, gnoemoe, openbve, fceu.
  * Update the blacklist: Add jag-data, lugaru-data,
    minetest-mod-character-creator, minetest-mod-quartz and planetblupi-common
    to blacklist.
  * tetris: Add quadrapassel and Suggest kblocks.
    Thanks to Jeremy Bicha for the report. (Closes: #885044)
  * Remove alienblaster from finest, finest-light task because we already
    recommend chromium-bsu.

 -- Markus Koschany <apo@debian.org>  Sun, 24 Dec 2017 15:14:40 +0100

debian-games (2.1) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.0.1.
  * New packages:
    - arcade: mrboom
    - rogue: lambdahack
    - emulator: gnome-video-arcade (Closes: #869284)
  * Removed packages: (removed from Debian)
    - content-dev: aseprite
    - python2-dev,python3-dev: pida
  * Run make dist.

 -- Markus Koschany <apo@debian.org>  Thu, 17 Aug 2017 01:12:52 +0200

debian-games (2) unstable; urgency=medium

  * Removed packages: (removed from Debian)
    - card: cardstories.
    - puzzle: glotski.
  * Run make dist and update the debian control file.

 -- Markus Koschany <apo@debian.org>  Sun, 09 Apr 2017 17:00:27 +0200

debian-games (1.7) unstable; urgency=medium

  * Removed packages: (removed from Debian)
    - strategy: castle-combat.
  * Add minetest-mod-maidroid to blacklist because it is just a mod for
    minetest.
  * Run make dist.

 -- Markus Koschany <apo@debian.org>  Sat, 31 Dec 2016 21:43:52 +0100

debian-games (1.6) unstable; urgency=medium

  * New recommendations:
    - toys: bucklespring
    - board: gnudoq
  * Move csmash from arcade to simulation.
  * Add auto-generated games-all metapackage.
  * Add gearhead2-sdl and mednafen to blacklist because we already recommend
    another flavor of them.
  * Run make dist.

 -- Markus Koschany <apo@debian.org>  Sun, 27 Nov 2016 23:31:02 +0100

debian-games (1.5) unstable; urgency=medium

  * Switch to compat level 10.
  * New recommendations:
    - puzzle: cavepacker.
    - rpg: ember.
    - console: nudoku.
    - finest,platform: caveexpress.
    - sport: bygfoot.
  * New suggestions:
    - fps: ezquake.
  * Removed packages: (removed from Debian or other issues)
    - finest,finest-light,platform: smc.
    - programming, strategy: gnurobots.
    - arcade: icebreaker.
    - content-dev: krita.
  * finest, finest-light: Remove aisleriot.
    Two recommended card (solitaire) games are sufficient.
  * emulator: Replace mednafen with mednaffe.
  * finest: Replace angband with nethack-console. Nethack is better maintained
    and more popular.
  * c++-dev: Replace libphobos-4.9-dev with libphobos-dev.
  * java-dev: Recommend netbeans instead of eclipse.
  * copyright: Change contact address to Debian Games Team.
  * Update the blacklist.
    Minetest mods, data packages and libretro are not standalone games.
  * Add gnome-2048 to blacklist because it is GNOME specific.
  * emulator: Remove transitional mess package.
  * Update README.source and note that the auto-generated games-all package
    has to be removed until a config option for blends-dev exists.

 -- Markus Koschany <apo@debian.org>  Sun, 25 Sep 2016 21:14:51 +0200

debian-games (1.4) unstable; urgency=medium

  * New recommendations:
    - python2-dev: python-pygame-sdl2.
    - console: colossal-cave-adventure.
    - arcade: vor.
    - emulator: mgba-sdl, osmose-emulator, retroarch, mame and mess.
    - adventure: Suggest scummvm-tools.
  * Removed packages: (removed from Debian or other issues)
    - board,chess: yics.
    - chess: glchess.
    - card: jpoker.
  * Declare compliance with Debian Policy 3.9.8.
  * README.source: Explain how to change recommendations.
  * Update src/blacklist. Add werewolf because it is only a game engine.
  * finest, finest-light: Replace xboard with dreamchess.
    xboard is an excellent chess variant but I feel that dreamchess is better
    suited for casual gamers who might prefer more eye-candy.
  * finest, finest-light: Replace angrydd with blockattack.
    Blockattack is another tetris-like game. I think it is better suited for
    finest and finest-light because it also supports two-player games and it is
    actively developed. It will most likely support SDL2 soon.
  * Make games-finest available in Debian's installer by setting Install: true
    in tasks/finest.
    Thanks to Ole Streicher for the report. (Closes: #825182)

 -- Markus Koschany <apo@debian.org>  Sat, 04 Jun 2016 02:07:44 +0200

debian-games (1.3) unstable; urgency=medium

  * Team upload.
  * Change recommendation from libpng12-dev to libpng-dev. (Closes: #816117)

 -- Tobias Frost <tobi@debian.org>  Mon, 28 Mar 2016 13:42:43 +0200

debian-games (1.2) unstable; urgency=medium

  * New recommendations:
    - content-dev: Switch from ardour3 to ardour package.
    - platform: asylum.
    - finest, strategy: ufoai.
    - adventure: Depend on renpy-thequestion instead of renpy.
    - adventure: zoom-player.
    - arcade: Switch from heroes-sdl to heroes.
    - platform: edgar.
    - strategy: endless-sky.
  * Removed packages: (removed from Debian or other issues)
    - finest, finest-light: triplea because it depends on obsolete
      commons-httpclient library.
    - finest, finest-light: boswars, since it has some flaws and upstream
      development is slow.
    - rpg: balazar.
    - arcade: balazarbrother.
    - strategy: freecraft.
    - programming, strategy: realtimebattle.
    - python2-dev: python-soya.
    - perl-dev: padre.
  * fps: Suggest eureka and deutex from now on.
  * Vcs-Git: Switch to https.
  * Declare compliance with Debian Policy 3.9.7.

 -- Markus Koschany <apo@debian.org>  Sun, 14 Feb 2016 14:20:48 +0100

debian-games (1.1) unstable; urgency=medium

  [ Andreas Tille ]
  * Added dependency data as created by blends-dev_gsoc to maintain future
    changelogs and package statistics.

  [ Markus Koschany ]
  * Vcs-Browser: Use https.
  * Improve package description of games-console to ensure that text console
    games are not confused with requiring a video game console for playing.
    Thanks to Takahide Nojima for the report. (Closes: #793573)
  * New recommendations:
    - emulator: cen64, gngb, dolphin-emu, mupen64plus-ui-console
    - toys: bambam and bb
    - education: granule
    - arcade: mrrescue and wizznic
    - python2-dev: python-renpy
    - fps: rbdoom3bfg
    - chess: shogivar, sjaakii and uci2wb
    - puzzle: wizznic
    - typing: speedpad
  * Removed packages: No longer in Debian.
    - chess: emacs-chess
    - perl-dev: libogre-perl
    - card: mathwar
    - puzzle: mathwar
    - typing: lletters
    - c++-dev: libois-dev. No longer developed.
    - puzzle: scribble
    - mud: pennmush
  * Only suggest texlive-games.
  * simulation: Suggest bsdgames instead of recommending them.
  * Depend on freeciv metapackage instead of freeciv-client-gtk.
  * Add python helper scripts to source package.
    Those helper scripts are useful for getting a quick overview about
    - what games (binary packages) do exist in Debian?
    - what games are already listed in our task files?
    - what games are new?
  * Add README.Debian file.

 -- Markus Koschany <apo@debian.org>  Mon, 02 Nov 2015 19:45:03 +0100

debian-games (1) unstable; urgency=medium

  * Update debian/control and synchronize metapackages with current available
    packages in testing.
  * c++-dev:
    - Recommend libopenscenegraph-dev.
    - Replace libjpeg8-dev with libjpeg-dev.
    - Drop crystalspace and libcrystalspace-dev because they are
      RC buggy.
  * chess:
    - Recommend chessx.
  * card:
    - Only suggest jpoker because it depends on apache2 which is an undesired
      dependency.

 -- Markus Koschany <apo@gambaru.de>  Mon, 30 Mar 2015 18:06:50 +0200

debian-games (0.11) unstable; urgency=medium

  * Declare compliance with Debian Policy 3.9.6.
  * Update Vcs-Browser field to new canonical address.
  * Sort all task files alphabetically.
  * Update README.source. Mention task file ordering.
  * Fix lintian warning in c++-dev (line was too long).
  * Shorten the short description of content-dev.
  * perl-dev: Suggest pangzero.
  * perl-dev: Suggest padre as an IDE for Perl.
  * adventure: Remove freesci. It is going to be removed from Debian.
  * c++-dev: Recommend libpng12-dev and libtiff5-dev instead of their virtual
    packages.
  * card: Suggest gnome-games and kdegames.
  * Add powermanga to finest and finest-light task.
  * Rename flare to flare-game because the package was renamed.
  * Add tecnoballz to finest and finest-light task.
  * Remove xgalaga from finest and finest-light task.
    A couple of great shoot' em up games are already recommended and the task
    provides similar alternatives like powermanga or open-invaders.
  * Add xmoto to racing task.
  * Add solarwolf to finest and finest-light task.
  * Add python-sdl2 to python2-dev.
  * Add python3-sdl2 to python3-dev.
  * Add tiled, a general purpose tile map editor, to content-dev.
  * emulator: Suggest wine.
  * content-dev: Replace ardour with ardour3.

 -- Markus Koschany <apo@gambaru.de>  Thu, 23 Oct 2014 23:01:27 +0200

debian-games (0.10) unstable; urgency=medium

  [ Markus Koschany ]
  * Add shootemup task. Promote Debian's plethora of shmup games.
  * Add minesweeper task. Thanks to Axel Beckert for the suggestion.
    (Closes: #757029)
  * Add programming task to increase the visibility of educational programming
    games.
  * Add toys task and recommend desktop toys and funny console software.
  * Add education task to promote educational software and games for children.
  * Add content-dev task with a selection of suitable tools and packages for
    developing and editing game content like graphics, audio and video
    material. Thanks to Paul Wise for the suggestions.
  * Add developer tasks to facilitate the development of games in different
    programming languages:
    - Add java-dev task.
    - Add python2-dev task.
    - Add python3-dev task.
    - Add c++-dev task.
    - Add perl-dev task.
  * Add dangen to arcade task.
  * Add ricochet to board task.
    Thanks to Axel Beckert for the report (Closes: #757047)
  * Add freecraft to strategy task.
  * Rename xblast to xblast-tnt. The former one is a transitional package.
  * Update games-fps task.
    Remove engines which are pulled in by already recommended games.
    - cube2
    - prboom-plus
    - darkplaces
    - ioquake3
    Suggest quakespasm since game data has not been packaged yet.
    Suggest doomsday since prboom-plus is the preferred engine for freedoom.
    Thanks to Fabian Greffrath for the report. (Closes: #757094)
  * Add adanaxisgpl to arcade task.
  * Add armagetronad and gltron to racing task.
  * Add 2048-qt to puzzle task.
  * Add aajm to console task.
  * Add balazarbrothers to arcade task.
  * Suggest cardstories in card task.
  * Add cavezofphear to console task.
  * Improve package description for games-fps.
  * Add crrcsim to simulation task.
  * Suggest efp in emulator task.
  * Suggest fceu and add fceux to emulator task.
  * Add ffrenzy to platform task.
  * Add flare to rpg task. Thanks to Manuel A. Fernandez Montecelo for the
    report. (Closes: #757403)
  * Suggest fofix in arcade task.
  * Suggest kdegames and gnome-games in arcade, board, chess and puzzle task.
  * Add freealchemist to puzzle and tetris tasks.
  * Suggest gbrainy in puzzle task.
  * Add gpsshogi to board task.
  * Add hachu to board and chess task.
  * Add instead to adventure task.
  * Add jmdlx to arcade task.
  * Add lightsoff to puzzle task.
  * mud and rpg task: Suggest minetest.
    Minetest is not a real mud game but it shares a few similarities.
  * simulation task: Suggest linthesia.
  * Add miceamaze to puzzle task.
  * Suggest netrek-client-cow in arcade, simulation and strategy tasks.
  * Add openbve to simulation.
  * Add pax-britannica to strategy task.
  * Add pegsolitaire to board task.
  * Add randtype to typing task.
  * Suggest scribble in puzzle task.
  * Add starfighter to arcade.
  * Add wordwarvi to arcade.
  * Add xmahjongg to board task.
  * Add xracer to racing.
  * Add xword to puzzle task.
  * card task: Suggest yahtzeesharp.
  * Add zivot to console task.
  * Recommend oolite in simulation and strategy tasks.
  * Remove xshisen from finest and finest-light task.
  * Add flare to finest task.
  * Add mirrormagic to arcade task.
  * Add abe to platform task.
  * Remove openclonk from finest-light. It requires a hardware accelerated
    video card.
  * Suggest efp in arcade task.
  * Suggest colobot in strategy task.
  * Puzzle task: Recommend gbrainy.
  * Add README.source file.
  * Add fs-uae-arcade and hatari to emulator task.
  * Suggest lightspeed in simulation task.
  * Add virtualjaguar to emulator task.
  * Add gnurobots to programming task.

  [ Tobias Hansen ]
  * Add libalure-dev to c++-dev task.

 -- Markus Koschany <apo@gambaru.de>  Fri, 05 Sep 2014 14:46:54 +0200

debian-games (0.9) unstable; urgency=medium

  * Initial release.

 -- Markus Koschany <apo@gambaru.de>  Mon, 04 Aug 2014 10:51:10 +0200
